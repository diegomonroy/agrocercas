<!-- Begin Bottom -->
	<section class="bottom" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'bottom' ); ?>
			</div>
		</div>
	</section>
<!-- End Bottom -->