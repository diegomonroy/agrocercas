<!-- Begin Top 1 -->
	<section class="top_1 hide-for-small-only" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
		</div>
	</section>
<!-- End Top 1 -->
<!-- Begin Top 2 -->
	<section class="top_2" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_2' ); ?>
			</div>
		</div>
	</section>
<!-- End Top 2 -->
<!-- Begin Top 3 -->
	<section class="top_3" data-wow-delay="0.5s">
		<div class="row collapse expanded align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_3' ); ?>
			</div>
		</div>
	</section>
<!-- End Top 3 -->